﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.NetSync.L2_SyncMaster {
	public interface IdGenerator {
		int GetUniqueId();
	}
}
