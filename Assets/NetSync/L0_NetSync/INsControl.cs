﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetSync {

	public interface INsControl {
		// Connects the control to other controls
		void Start();
		// Disconnects this control from the other controls
		void Stop();
		// Handle network data by sending it to local
		void ReadFromNet();
		// Handle local data by sending it to net
		void WriteToNet();
		// Register a new object to be produced over the control space
		// Used to indicate an object should be public over the network
		void RegisterSyncable(Object toRegister);
		// Unregister a new object to remove it from the control space
		void UnregisterSyncable(Object toUnregister);
		// OnRegister event
		event OnRegisterDelegate OnRegister;
	}
}
