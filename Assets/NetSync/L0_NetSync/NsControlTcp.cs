﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Assets.NetSync.L1_Networking;

namespace NetSync {
	public class NsControlTcp : INsControl {
		public short Port { get; private set; }
		public string Host { get; private set; }

		public bool IsConnectedToServer { get { return _myClient != null && _myClient.IsConnected; } }

		public bool IsServerRunning { get { return _myServer != null && _myServer.IsConnected; } }
		private readonly bool _runAsServer = false;
		private INsMaster _myMaster;
		private INetServer _myServer = null;
		private INetClient _myClient = null;

		// Client only
		/// <summary>
		/// Client constructor.
		/// </summary>
		/// <param name="host">The host address to connect to</param>
		/// <param name="port">The port to connect to host</param>
		public NsControlTcp( string host, short port ) {
			Host = host;
			Port = port;
			_runAsServer = false;
		}

		// Client and server
		/// <summary>
		/// Server constructor.
		/// </summary>
		/// <param name="port">The port to connect to this server</param>
		public NsControlTcp( short port ) {
			Host = "localhost";
			Port = port;
			_runAsServer = true;
		}

		/// <summary>
		/// Start up the control, starting server if flagged, and connecting client.
		/// </summary>
		public void Start() {
			if( _runAsServer ) {
				_myServer = new TcpNetServer(port: Port);
				_myServer.Start();
			}
			_myClient = new TcpNetClient(host: Host, port: Port);
			_myClient.Start();
			_myMaster = new NsMaster( _myClient );
		}

		/// <summary>
		/// Stop the control, stopping both client and server if running.
		/// </summary>
		public void Stop() {
			if( _myClient != null ) _myClient.Stop();
			if( _myServer != null ) _myServer.Stop();
		}

		/// <summary>
		/// Check for new messages from server and queue in master.
		/// Responsible for deserializing messages from net.
		/// </summary>
		public void ReadFromNet() {
			// Get messages from network
			Packet newPacket = _myClient.ReadData();
			while ( newPacket != Packet.NullPacket ) {
				switch( newPacket.Type ) {
					case Packet.MyRoutingType.Broadcast:
						var msg = NsSyncMessage.Deserialize( newPacket.Data );
						_myMaster.SyncIn( msg );
						break;
					case Packet.MyRoutingType.Disconnect:
						// TODO: Will need to destroy all syncable's associated with the
						// disconnecting client
						break;
				}
				newPacket = _myClient.ReadData();
			}
		}

		/// <summary>
		/// Writes all messages queued in master to net.
		/// Responsible for serializing messages to send over net.
		/// </summary>
		public void WriteToNet() {
			// Get messages from master
			var dataToWrite = _myMaster.GetUnsyncedData();

			Debug.Assert( _myClient != null, "Hey pal, _myClient is null! Did you forget to start the client buddy?" );
			Debug.Assert(_myClient.IsConnected == true, "Client not connected");

			// Send messages to network
			foreach( var message in dataToWrite ) {
				_myClient.SendData( message.Serialize() );
			}
		}

		/// <summary>
		/// Register a synchable with our master
		/// </summary>
		/// <param name="val">The object to register as synched</param>
		public void RegisterSyncable( object val ) {
			_myMaster.RegisterSynchable( val );
		}

		/// <summary>
		/// Unregister object from master.
		/// </summary>
		/// <param name="val">A reference to the object to unregister.</param>
		public void UnregisterSyncable( object val ) {
			_myMaster.UnregisterNetSynchable( val );
		}

		/// <summary>
		/// Override the default master with a new implemention. Used for testing.
		/// </summary>
		/// <param name="val">The master implemenation to override with.</param>
		public void SetMaster( INsMaster val ) {
			_myMaster = val;
		}

		/// <summary>
		/// Override the default client with a new implementation. Used for testing.
		/// </summary>
		/// <param name="val">The client implementation to override with.</param>
		public void SetClient( INetClient val ) {
			_myClient = val;
		}

		// OnRegister event
		public event OnRegisterDelegate OnRegister {
			add { _myMaster.OnRegister += value; }
			remove { _myMaster.OnRegister -= value; }
		}
	}
}
