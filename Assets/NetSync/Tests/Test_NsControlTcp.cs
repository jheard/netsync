using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using NetSync;
using NUnit.Framework;
using Moq;

namespace Assets.NetSync.Tests {
	[TestFixture]
	public class Test_NsControlTcp {

		private const int NetworkDelayTime = 200;

		// start fake server
		private TcpNetServer CreateFakeServer() {
			var testServer = new TcpNetServer( 5432 );
			testServer.Start();
			return testServer;
		}

		[Test]
		public void Constructor_GivenHostAndPort_HostAndPortAreCorrect() {
			var control = new NsControlTcp( "testhost", 5432 );
			Assert.That( control.Host, Is.EqualTo( "testhost" ) );
			Assert.That( control.Port, Is.EqualTo( 5432 ) );
		}

		[Test]
		public void Constructor_GivenPortAlone_PortAndHostAreCorrect() {
			var control = new NsControlTcp( 5432 );
			Assert.That( control.Host, Is.EqualTo( "localhost" ) );
			Assert.That( control.Port, Is.EqualTo( 5432 ) );
		}

		[Test]
		public void Start_GivenServerConstructor_ServerIsRunning() {
			var control = new NsControlTcp( 5432 );
			control.Start();

			Thread.Sleep( NetworkDelayTime );

			Assert.That( control.IsServerRunning, Is.True );

			control.Stop();
		}

		[Test]
		public void Start_GivenTestServerAndClientConstructor_ServerIsNotRunning() {
			/*
			var testServer = CreateFakeServer();
			var control = new NsControlTcp( "localhost", 5432 );
			control.Start();

			Thread.Sleep( NetworkDelayTime );

			Assert.That( control.IsServerRunning, Is.False );

			testServer.Stop();
			 */
		}

		[Test]
		public void Start_GivenClient_ConnectedToTestServer() {
			var testServer = CreateFakeServer();
			// start test client
			var control = new NsControlTcp( "localhost", 5432 );
			control.Start();

			Thread.Sleep( NetworkDelayTime );

			Assert.That( control.IsConnectedToServer, Is.True, "Not showing as connected to server" );
			Assert.That( testServer.ConnectedClientCount, Is.EqualTo( 1 ) );

			testServer.Stop();
		}

		[Test]
		public void Start_GivenServer_ClientShowsConnected() {
			// test server/client
			var control = new NsControlTcp( 5432 );
			control.Start();

			Thread.Sleep( NetworkDelayTime );

			Assert.That( control.IsConnectedToServer, Is.True, "Not showing as connected to server" );

			control.Stop();
		}

		[Test]
		public void Stop_GivenClientStartFollowedByStop_ClientIsNotConnected() {
			var testServer = CreateFakeServer();

			// start test client
			var control = new NsControlTcp( "localhost", 5432 );
			control.Start();

			Thread.Sleep( NetworkDelayTime );

			control.Stop();

			Thread.Sleep( NetworkDelayTime );

			Assert.That( control.IsConnectedToServer, Is.False, "Showing as connected to server after stop" );

			testServer.Stop();
		}

        [Test]
        [Repeat(20)]
        public void Stop_GivenServerStartFollowedByStop_ClientIsNotConnectedAndServerNotRunning()
        {
            // start test server
			var control = new NsControlTcp( 5432 );
			control.Start();

			Thread.Sleep( NetworkDelayTime );

			control.Stop();

			Thread.Sleep( NetworkDelayTime );

			//Assert.That( control.IsConnectedToServer, Is.False, "Showing as connected to server after stop" );
			//Assert.That( control.IsServerRunning, Is.False, "Showing server as running after stop" );
		}

		[Test]
		public void Register_OnRegisterObjectWithControl_NsMasterRegisterIsCalled() {
			/*
			var syncable = new object();
			var moqMaster = new Mock<INsMaster>();
			NsControlTcp control = new NsControlTcp( 5432 );
			control.SetMaster( moqMaster.Object );
			control.RegisterSyncable( syncable );
			moqMaster.Verify( m => m.RegisterSynchable( syncable ), Times.Exactly( 1 ) );
			 */
		}

		[Test]
		public void Register_OnRegisterObjectWithControlThenUnregister_NsMasterUnregisterIsCalled() {
			var syncable = new object();
			var moqMaster = new Mock<INsMaster>();
			NsControlTcp control = new NsControlTcp( 5432 );
			control.SetMaster( moqMaster.Object );
			control.RegisterSyncable( syncable );
			control.UnregisterSyncable( syncable );
			moqMaster.Verify( m => m.UnregisterNetSynchable( syncable ), Times.Exactly( 1 ) );
		}

		[Test]
		public void Write_Given2MessagesInUnsynchedData_OnWriteGetUnsynchedDataAndSendMessagesIsCalled() {
			var syncable = new object();
			var moqMaster = new Mock<INsMaster>();
			var moqClient = new Mock<INetClient>();
			var control = new NsControlTcp( 5432 );

			// configure moqs
			moqMaster.Setup( master => master.GetUnsyncedData() )
				.Returns( new List<NsSyncMessage>() {
					new NsSyncMessageNew( 0, new object(), false ),
					new NsSyncMessageNew( 1, new object(), false )
				} );
			moqClient.Setup(client => client.IsConnected).Returns(true);
			
			// set our mocks
			control.SetMaster( moqMaster.Object );
			control.SetClient( moqClient.Object );

			// test method
			control.WriteToNet();
			moqMaster.Verify( m => m.GetUnsyncedData(), Times.Exactly( 1 ) );
			moqClient.Verify( m => m.SendData( It.IsAny<string>() ), Times.Exactly( 2 ) );
		}

		[Test]
		public void Read_OnClientReturns3PacketsThenNullPacket_ReadDataCalled4Times() {
			var syncable = new object();
			var moqMaster = new Mock<INsMaster>();
			var moqClient = new Mock<INetClient>();
			var control = new NsControlTcp( 5432 );

			// configure moqs
			Queue<Packet> fakeClientPackets = new Queue<Packet>();
			fakeClientPackets.Enqueue( new Packet( Packet.MyRoutingType.Disconnect, 0, "0" ) );
			fakeClientPackets.Enqueue(new Packet(Packet.MyRoutingType.Disconnect, 0, "1"));
			fakeClientPackets.Enqueue(new Packet(Packet.MyRoutingType.Disconnect, 0, "2"));
			fakeClientPackets.Enqueue( Packet.NullPacket );
			moqClient.Setup( client => client.ReadData() ).Returns( fakeClientPackets.Dequeue );

			// set our mocks
			control.SetMaster( moqMaster.Object );
			control.SetClient( moqClient.Object );

			// test method
			control.ReadFromNet();
			moqClient.Verify( client => client.ReadData(), Times.Exactly( 4 ) );
		}

		[Test]
		public void Read_OnClientReturns1UserDataPacket_MasterSyncInCalledWithCorrectMessage() {
			var syncable = new object();
			var moqMaster = new Mock<INsMaster>();
			var moqClient = new Mock<INetClient>();
			var control = new NsControlTcp(5432);

			// configure moqs
			Queue<Packet> fakeClientPackets = new Queue<Packet>();
			var msg = new NsSyncMessageDelete(0);
			fakeClientPackets.Enqueue(new Packet(Packet.MyRoutingType.Broadcast, 0, msg.Serialize()));
			// Add null packet to signify end of packet list
			fakeClientPackets.Enqueue(Packet.NullPacket);
			moqClient.Setup(client => client.ReadData()).Returns(fakeClientPackets.Dequeue);

			// set our mocks
			control.SetMaster(moqMaster.Object);
			control.SetClient(moqClient.Object);

			// test method
			control.ReadFromNet();
			moqMaster.Verify(master => master.SyncIn(msg), Times.Exactly(1));
		}

		/// <summary>
		/// Class used by test to validate new object registration event
		/// </summary>
		class RegistrationValidator {
			public object LastRegistered = null;
			public void ValidateRegister(object newRegistered) {
				LastRegistered = newRegistered;
			}
		}

		
		[Test]
		public void OnRegister_GivenLocalDelegate_DelegateIsCalled() {
			object newRegisteredToValidate;
			var listener = new RegistrationValidator();

			var control = new NsControlTcp(5432); // sut
			var syncable = new object(); // object to register

			control.Start();
			control.OnRegister += listener.ValidateRegister;
			control.RegisterSyncable( syncable );
			control.WriteToNet();
			control.ReadFromNet();
			Assert.That( listener.LastRegistered, Is.EqualTo( syncable ) );
			control.Stop();
		}
		 

		class Dummy { }

		[Test]
		public void OnRegister_GivenRemoteDelegate_DelegateIsCalled() {
			object newRegisteredToValidate;
			var listener = new RegistrationValidator();

			var serverControl = new NsControlTcp(5432); // sut
			serverControl.Start();

			var syncable = new Dummy(); // object to register

			var clientControlToTest = new NsControlTcp( host: "localhost", port: 5432 );

			// create our client, register our test delegate
			clientControlToTest.Start();
			clientControlToTest.OnRegister += listener.ValidateRegister;
			Thread.Sleep(NetworkDelayTime);

			// register the syncable to be transmitted to the client
			serverControl.RegisterSyncable(syncable);
			serverControl.WriteToNet();
			Thread.Sleep(NetworkDelayTime);

			clientControlToTest.ReadFromNet();
			Assert.IsInstanceOf<Dummy>(listener.LastRegistered);

			serverControl.Stop();
		}
	}
}

