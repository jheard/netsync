using System.Net.Sockets;
using System.Threading;
using NetSync;
using NUnit.Framework;
using System.Diagnostics;

namespace Assets.NetSync.Tests
{
	[TestFixture]
	public class Test_NetClient {
		private const int NetworkDelayTime = 500;
		private const string Host = "127.0.0.1";
		private const short Port = 15522;

		private TcpNetServer _server;

		[SetUp]
		public void SetUp()
		{
			_server = new TcpNetServer(Port);
			_server.Start();
		}

		[TearDown]
		public void TearDown()
		{
			_server.Stop();
		}
		
		[Test]
		public void GivenServerAndClient_ClientCanConnectToServer() {
			var testClient = new TcpNetClient (Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);
			Assert.That (_server.ConnectedClientCount, Is.EqualTo (1));
			Assert.That (testClient.IsConnected, Is.True);
		}

		[Test]
		public void IfNoData_ClientReadsNull()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);
			//testClient.ReadData();
			var receivedData = testClient.ReadData();
			Assert.That(receivedData.DestinationId, Is.EqualTo(Packet.NullDestination));
		}

		[Test]
		public void GivenClientSendPacket()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);
			testClient.SendData ("Hello world");
			Thread.Sleep(NetworkDelayTime);
			var receivedData = testClient.ReadData();
			Assert.That(receivedData.Data, Is.EqualTo("Hello world"));
		}

		[Test]
		public void GivenClientSendPacketOnlyOnce()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);
			testClient.SendData("Hello world");
			Thread.Sleep(NetworkDelayTime);
			var receivedData = testClient.ReadData();
			Thread.Sleep(NetworkDelayTime);
			receivedData = testClient.ReadData();
			Assert.That(receivedData.Type, Is.EqualTo(Packet.MyRoutingType.Null));
		}

		[Test]
		public void GivenClientSends2Packets_RecieveExactlyTwoPacketsInCorrectOrder()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);
			testClient.SendData("test data 1");
			testClient.SendData("test data 2");
			Thread.Sleep(NetworkDelayTime);
			var receivedData = testClient.ReadData();
			Assert.That(receivedData.Data, Is.EqualTo("test data 1"));
			receivedData = testClient.ReadData();
			Assert.That(receivedData.Data, Is.EqualTo("test data 2"));
			receivedData = testClient.ReadData();
			Assert.That(receivedData.Type, Is.EqualTo(Packet.MyRoutingType.Null));
		}

		[Test]
		public void GivenClientStopCalled_ClientIsConnectIsFalse()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			testClient.Stop();
			Assert.That(testClient.IsConnected, Is.EqualTo(false));
		}

		[Test]
		public void GivenClientStopCalled_ClientIsDisconnectedFromServer()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);

			testClient.Stop();
			Thread.Sleep(NetworkDelayTime);

			Assert.That(_server.ConnectedClientCount, Is.EqualTo(0));
		}

		[Test]
		public void GivenClientGetUniqueIdCalledTwice_ReceivesTwoDifferentNumbers()
		{
			var testClient = new TcpNetClient(Host, Port);
			testClient.Start();
			Thread.Sleep(NetworkDelayTime);

			int id1 = testClient.GetUniqueId();
			int id2 = testClient.GetUniqueId();

			testClient.Stop();
			Thread.Sleep(NetworkDelayTime);

			Assert.That(id1, Is.Not.EqualTo(id2));
		}
	}
}
