using System;
using System.Linq;
using Assets.NetSync.L2_SyncMaster;
using NetSync;
using NUnit.Framework;

namespace Assets.NetSync.Tests {
	public class FakeIdGenerator : IdGenerator {
		public int GetUniqueId() {
			return ++curId;
		}

		private int curId;
	}

	[TestFixture]
	public class TestNsMaster {
		private NsMaster _master;

		[SetUp]
		public void Setup() {
			_master = new NsMaster( new FakeIdGenerator()) ;
		}

		private class NetSyncTest1 {
			[NsSyncableMember]
			public int data1;
			[NsSyncableMember]
			public string data2;

			public float data3;

			[NsSyncableMember]
			public float[] data4;
		}

		public void CalibrateMaster(NsMaster master) {
			var unsynced = master.GetUnsyncedData();
			foreach (var syncItem in unsynced) {
				master.SyncIn(syncItem);
			}
		}

		[Test]
		public void GivenRegisteredNetSyncableChanges_GetUnsyncedDataReturnsProperData() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);
			CalibrateMaster(_master);
			netSync.data1 = 35;
			var dataChangeList = _master.GetUnsyncedData();

			Assert.That(dataChangeList.Count, Is.EqualTo(1));
			var netSyncData = (NsSyncMessageChange)dataChangeList[0];
			Assert.That(netSyncData.MemberToSync.Length, Is.EqualTo(1));
			Assert.That(netSyncData.MemberToSync[0], Is.EqualTo("data1"));
			Assert.That(netSyncData.Value, Is.EqualTo(35));
		}

		[Test]
		public void Given2RegisteredNetSyncableChanges_GetUnsyncedDataReturnsProperData() {
			var netSync1 = new NetSyncTest1();
			var netSync2 = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync1);
			_master.RegisterSynchable(netSync2);
			CalibrateMaster(_master);
			netSync1.data1 = 35;
			netSync2.data2 = "hello";
			var dataChangeList = _master.GetUnsyncedData();

			Assert.That(dataChangeList.Count, Is.EqualTo(2));
			var netSyncData1 = (NsSyncMessageChange)dataChangeList[0];
			var netSyncData2 = (NsSyncMessageChange)dataChangeList[1];

			Assert.That(
					netSyncData1.WrapperId,
					Is.Not.EqualTo(netSyncData2.WrapperId));

			Assert.That(netSyncData1.MemberToSync.Length, Is.EqualTo(1));
			Assert.That(netSyncData2.MemberToSync.Length, Is.EqualTo(1));

			if (netSyncData1.MemberToSync[0] == "data1") {
				Assert.That(netSyncData1.MemberToSync[0], Is.EqualTo("data1"));
				Assert.That(netSyncData1.Value, Is.EqualTo(35));

				Assert.That(netSyncData2.MemberToSync[0], Is.EqualTo("data2"));
				Assert.That(netSyncData2.Value, Is.EqualTo("hello"));
			} else if (netSyncData1.MemberToSync[0] == "data2") {
				Assert.That(netSyncData1.MemberToSync[0], Is.EqualTo("data2"));
				Assert.That(netSyncData1.Value, Is.EqualTo("hello"));

				Assert.That(netSyncData2.MemberToSync[0], Is.EqualTo("data1"));
				Assert.That(netSyncData2.Value, Is.EqualTo(35));
			} else {
				throw (new Exception("Data was invalid"));
			}
		}

		[Test]
		public void GivenUnregisterNetsynchable_GetUnsynchedDataReturnsDeleteMessage() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);
			CalibrateMaster(_master);
			_master.UnregisterNetSynchable(netSync);
			// we should only have one item of unsynched data
			var message = _master.GetUnsyncedData().First();
			Assert.That(message, Is.TypeOf<NsSyncMessageDelete>());
		}

		[Test]
		public void GivenUnregisteredNetsyncGetsChanged_GetUnsynchedDataDoesNotReturnChange() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);
			CalibrateMaster(_master);
			_master.UnregisterNetSynchable(netSync);
			// we should only have one item of unsynched data
			_master.SyncIn(_master.GetUnsyncedData().First());

			// data change on unregistered netsync
			netSync.data1 = 35;
			var dataChangeList = _master.GetUnsyncedData();

			Assert.That(dataChangeList.Count, Is.EqualTo(0));
		}

		[Test]
		public void GivenSyncIn_DataSynchs() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);
			CalibrateMaster(_master);

			// assuming uniqueId's start at 1
			var testMessage = new NsSyncMessageChange(1, new string[] { "data1" }, 35);

			_master.SyncIn(testMessage);

			// The data field should have changed value
			Assert.That(netSync.data1, Is.EqualTo(35));
			// There should be no unsynched values
			Assert.That(_master.GetUnsyncedData().Count, Is.EqualTo(0));
		}

		[Test]
		public void GivenSyncInForUnsyncedData_DataSyncs() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);
			CalibrateMaster(_master);
			netSync.data1 = 12;

			var unsyncedData = _master.GetUnsyncedData();
			// There should be one unsynched value
			Assert.That(unsyncedData.Count, Is.EqualTo(1));
			// Pass in everything to by synced
			foreach (var unsynced in unsyncedData) {
				_master.SyncIn(unsynced);
			}
			// There should be no unsynched values
			Assert.That(_master.GetUnsyncedData().Count, Is.EqualTo(0));
		}


		[Test]
		public void UnregisterSynchable_GivenUnregister_GetUnsyncedDataHasUnregisterSyncAsFirstItem() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);
			CalibrateMaster(_master);

			_master.UnregisterNetSynchable(netSync);
			var unsyncedData = _master.GetUnsyncedData();

			// There should be one unsynched value
			Assert.That(unsyncedData[0], Is.TypeOf<NsSyncMessageDelete>());
		}

		[Test]
		public void GivenNewRegister_GetUnsyncedDataHasNewSyncAsFirstItem() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);

			var unsyncedData = _master.GetUnsyncedData();
			// There should be one unsynched value
			Assert.That(unsyncedData[0], Is.TypeOf<NsSyncMessageNew>());
		}


		[Test]
		public void SyncIn_GivenNewMessageAndSyncIn_MessageIsRemovedFromSender() {
			var netSync = new NetSyncTest1();
			
			_master.RegisterSynchable(netSync);

			_master.SyncIn(_master.GetUnsyncedData().First());
			// There should be one unsynched value
			var actual = _master.GetUnsyncedData();
			Assert.That(actual, Has.None.TypeOf<NsSyncMessageNew>());
		}

		[Test]
		public void SyncIn_GivenNewMessageAndSyncIn_SyncedObjectIsCorrectType() {
			var netSync = new NetSyncTest1();
			
			_master.OnRegister += OnRegister;

			_master.RegisterSynchable(netSync);

			_master.SyncIn(_master.GetUnsyncedData().First());

			Assert.That(_toCheck, Is.TypeOf<NetSyncTest1>());
		}

		private Object _toCheck;
		private void OnRegister(Object o) {
			_toCheck = o;
		}
	}
}

