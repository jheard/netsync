using System;
using System.Linq;
using NetSync;
using NUnit.Framework;

namespace Assets.NetSync.Tests
{
	[TestFixture]
	public class Test_NsSyncMessage {
		[Test]
		public void NewNsSyncMessageSerialize_GivenSerialize_GetExpectedString() {
			string toSend = "test";
			NsSyncMessage sut = new NsSyncMessageNew( 42, toSend, false );
			var actual = sut.Serialize();
			var activatorString = NsSyncMessageNew.CreateActivatorString( toSend );
			var expected = "n42_" + activatorString;
			Assert.That( actual, Is.EqualTo( expected ) );
		}

		[Test]
		public void NewNsSyncMessage_GivenNonDefaultInt_CreatesDefaultInt() {
			int toCreate = 30;
			var sut = new NsSyncMessageNew(0, toCreate, false);
			var actual = sut.GenerateNewSyncableObject();
			int expected = 0;
			Assert.That(actual, Is.TypeOf<int>());
			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void NewNsSyncMessageDeserialize_GivenDeserialize_GetExpectedMessage() {
			string toSend = "test";
			NsSyncMessage toRecreate = new NsSyncMessageNew(200, toSend, false);
			var serializedString = toRecreate.Serialize();
			var actual = NsSyncMessage.Deserialize( serializedString );
			Assert.That(actual, Is.EqualTo(toRecreate));
		}

		[Test]
		public void DeleteNsSyncMessageSerialize_GivenSerialize_GetExpectedString()
		{
			NsSyncMessage sut = new NsSyncMessageDelete(35);
			var actual = sut.Serialize();
			var expected = "d35_";
			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void DeleteNsSyncMessageSerialize_GivenDeserialize_GetExpectedMessage()
		{
			NsSyncMessage toRecreate = new NsSyncMessageDelete(200);
			var serializedString = toRecreate.Serialize();
			var actual = NsSyncMessage.Deserialize(serializedString);
			Assert.That(actual, Is.EqualTo(toRecreate));
		}

		[TestCase(89, new string[] { "propertyName", "subPropertyName" }, "yoyoyo")]
		public void ChangeNsSyncMessageSerialize_GivenSerialize_GetExpectedString(int wrapperId, string[] syncedAddress, object value)
		{
			NsSyncMessage sut = new NsSyncMessageChange(wrapperId, syncedAddress, value);
			var actual = sut.Serialize();
			var expected = "c" + wrapperId + "_propertyName.subPropertyName_" + value.ToString();
			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void ChangeNsSyncMessageSerialize_GivenDeserialize_GetExpectedMessage()
		{
			NsSyncMessage toRecreate = new NsSyncMessageChange(200, new String[] {"first", "second", "third"}, "hiyooo");
			var serializedString = toRecreate.Serialize();
			var actual = NsSyncMessage.Deserialize(serializedString);
			Assert.That(actual, Is.EqualTo(toRecreate));
		}
	}
}

