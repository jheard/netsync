using NetSync;
using NUnit.Framework;

namespace Assets.NetSync.Tests
{
	[TestFixture]
	public class Test_NsObjectHandler
	{
		class NetSyncTest1
		{
			[NsSyncableMember]
			public int data1;
			[NsSyncableMember]
			public string data2;

			public float data3;

			[NsSyncableMember]
			public float[] data4;
		}

		public void CalibrateWrapper(NsObjectHandler handler)
		{
			var unsynced = handler.GetUnsyncedData();
			foreach(var syncItem in unsynced)
			{
				handler.SyncIn((NsSyncMessageChange)syncItem);
			}
		}

		[Test]
		public void GivenNewWrapper_allSyncItemsStartUnsynced()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 0);
			var expected = 3; // NetSyncTest1 has 4 fields, but only 3 are synced
			var actual = wrapper.GetUnsyncedData().Count;
			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void GivenNewWrapper_allSyncItemsStartUnsyncedWithProperValues()
		{
			var netSync = new NetSyncTest1();
			netSync.data1 = 48;
			netSync.data2 = "hiyo";
			netSync.data3 = 3.14f;
			netSync.data4 = new float[] { 4.3f, 3.4f, 5.6f };
			var wrapper = new NsObjectHandler(netSync, 0);
			var messages = wrapper.GetUnsyncedData();
			foreach (var message in messages)
			{
				switch(message.MemberToSync[0])
				{
					case "data1": Assert.That(message.Value, Is.EqualTo(48)); break;
					case "data2": Assert.That(message.Value, Is.EqualTo("hiyo")); break;
				}
			}
		}

		[Test]
		public void GivenNoMemberOnWrappedClassChanged_GetUnsyncedDataReturnsZeroValues()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 0);
			CalibrateWrapper(wrapper);
			var expected = 0;
			var actual = wrapper.GetUnsyncedData().Count;
			Assert.That(actual, Is.EqualTo(expected));
		}

		[Test]
		public void GivenMemberChangedOnWrappedClass_GetUnsyncedDataReturnsCorrectValue()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 3);
			CalibrateWrapper(wrapper);
			netSync.data1 = 35;
			var dataChangeList = wrapper.GetUnsyncedData();

			Assert.That(dataChangeList.Count, Is.EqualTo(1));
			Assert.That(dataChangeList[0].WrapperId, Is.EqualTo(3));
			Assert.That(dataChangeList[0].MemberToSync.Length, Is.EqualTo(1));
			Assert.That(dataChangeList[0].MemberToSync[0], Is.EqualTo("data1"));
			Assert.That(dataChangeList[0].Value, Is.EqualTo(35));
		}

		[Test]
		public void GetUnsyncedData_DoesntSyncData()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 0);
			CalibrateWrapper(wrapper);
			netSync.data1 = 35;
			wrapper.GetUnsyncedData();
			var actual = wrapper.GetUnsyncedData().Count;
			Assert.That(actual, Is.EqualTo(1));
		}

		[Test]
		public void ChangeUnsyncData_DoesntAddToSyncList()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 0);
			CalibrateWrapper(wrapper);
			netSync.data3 = 21;
			var actual = wrapper.GetUnsyncedData().Count;
			Assert.That(actual, Is.EqualTo(0));
		}

		[Test]
		public void GivenSyncValueChangedToNull_IsRecognizedAsUnsynced()
		{
			var netSync = new NetSyncTest1();
			netSync.data2 = "hilo";
			var wrapper = new NsObjectHandler(netSync, 0);
			CalibrateWrapper(wrapper);
			netSync.data2 = null;
			var actual = wrapper.GetUnsyncedData().Count;
			Assert.That(actual, Is.EqualTo(1));
		}

		[Test]
		public void Given2MembersChangedOnWrappedClass_GetUnsyncedDataReturnsCorrectValues()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 3);
			CalibrateWrapper(wrapper);
			netSync.data1 = 35;
			netSync.data2 = "yoyoyo";
			var dataChangeList = wrapper.GetUnsyncedData();

			Assert.That(dataChangeList.Count, Is.EqualTo(2));

			Assert.That(dataChangeList[0].WrapperId, Is.EqualTo(3));
			Assert.That(dataChangeList[0].MemberToSync.Length, Is.EqualTo(1));
			Assert.That(dataChangeList[0].MemberToSync[0], Is.EqualTo("data1"));
			Assert.That(dataChangeList[0].Value, Is.EqualTo(35));

			Assert.That(dataChangeList[1].WrapperId, Is.EqualTo(3));
			Assert.That(dataChangeList[1].MemberToSync.Length, Is.EqualTo(1));
			Assert.That(dataChangeList[1].MemberToSync[0], Is.EqualTo("data2"));
			Assert.That(dataChangeList[1].Value, Is.EqualTo("yoyoyo"));
		}

		[Test]
		public void GivenCallSyncIn_ValueChangesAndMemberIsSynced()
		{
			var netSync = new NetSyncTest1();
			var wrapper = new NsObjectHandler(netSync, 0);
			CalibrateWrapper(wrapper);
			netSync.data1 = 35;
			wrapper.SyncIn(new NsSyncMessageChange(0, new string[] { "data1" }, 56));

			// Value has changed
			Assert.That(netSync.data1, Is.EqualTo(56));

			// Member is synced
			var syncCount = wrapper.GetUnsyncedData().Count;
			Assert.That(syncCount, Is.EqualTo(0));
		}

		//[Test]
		//public void GivenSyncedArrayElementChanged_GetUnSyncedReturnsIndexOfElement()
		//{
		//	var netSync = new NetSyncTest1();
		//	netSync.data4 = new float[] { 6, 12 };
		//	var handler = new NsObjectHandler(netSync, 3);
		//	netSync.data4[1] = 35;
		//	var dataChangeList = handler.GetUnsyncedData();


		//	Assert.That(dataChangeList.Count, Is.EqualTo(1));
		//	var netSyncData = dataChangeList[0];
		//	Assert.That(netSyncData.wrapperId, Is.EqualTo(3));
		//	Assert.That(netSyncData.memberToSync.Length, Is.EqualTo(2));
		//	Assert.That(netSyncData.memberToSync[0], Is.EqualTo("data4"));
		//	Assert.That(netSyncData.memberToSync[1], Is.EqualTo("1"));
		//	Assert.That(netSyncData.value, Is.EqualTo(35));
		//}
	}
}

