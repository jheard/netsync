using System;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using NetSync;
using NUnit.Framework;
using System.Diagnostics;

namespace Assets.NetSync.Tests {
	[TestFixture]
	public class TestNetServer {
		private const int NetworkDelayTime = 500;
		private const string Host = "127.0.0.1";
		private const short Port = 15522;

		private TcpNetServer _server;

		[SetUp]
		public void SetUp() {
				_server = new TcpNetServer(Port);
				_server.Start();
		}

		[TearDown]
		public void TearDown() {
				_server.Stop();
		}
		
		[Test]
		public void GivenServerStart_ServerIsConnectedAndHasZeroClientCount() {
			Assert.That (_server.IsConnected, Is.True);
			Assert.That(_server.ConnectedClientCount, Is.EqualTo(0));
		}

		
		[Test]
		public void ClientCanConnectToStartedServer() {
			// making sure generic tcpClient can connect to our server
			var testClient = new TcpClient(Host, Port);
			Assert.That(testClient.Connected);
			testClient.Close();
		}

		[Test]
		public void GivenConnectedClient_ReceiveGreetingPacketWithClientIdOfOne() {
			var testClient1 = new TcpClient(Host, Port);

			// increase client timeout for test
			testClient1.ReceiveTimeout = 2000;

			Thread.Sleep(NetworkDelayTime);

			var receivedGreeting = new byte[16];
			testClient1.GetStream().Read(receivedGreeting, 0, 16);

			Packet greetingPacket = new Packet(receivedGreeting);

			Assert.That(greetingPacket.Type, Is.EqualTo(Packet.MyRoutingType.ServerGreeting));
		}

		[Test]
		public void GivenThreeConnectedClients_AllReceiveMessageFromOne() {
			var testClient1 = new TcpClient(Host, Port);
			var testClient2 = new TcpClient(Host, Port);
			var testClient3 = new TcpClient(Host, Port);

			// increase client timeout for test
			testClient1.ReceiveTimeout
					= testClient2.ReceiveTimeout
					= testClient3.ReceiveTimeout = 2000;

			Thread.Sleep(NetworkDelayTime);

			// Process the greeting packet
			var receivedGreeting = new byte[16];
			testClient1.GetStream().Read(receivedGreeting, 0, 16);
			testClient2.GetStream().Read(receivedGreeting, 0, 16);
			testClient3.GetStream().Read(receivedGreeting, 0, 16);

			var packet = new Packet(Packet.MyRoutingType.Broadcast, 1, "TestMessage");
			var message = packet.Serialize();
			// handle generic tcpClient to do what our NetClient will eventually do
			testClient1.GetStream().Write(message, 0, message.Length);

			Thread.Sleep(NetworkDelayTime);
            
			// create 3 messages, populate from each test client
			var receivedMessage1 = new byte[message.Length];
			var receivedMessage2 = new byte[message.Length];
			var receivedMessage3 = new byte[message.Length];
			try {
					testClient1.GetStream().Read(receivedMessage1, 0, message.Length);
					testClient2.GetStream().Read(receivedMessage2, 0, message.Length);
					testClient3.GetStream().Read(receivedMessage3, 0, message.Length);

					var p = new Packet(receivedMessage1);

					// assert that all three received messages are the same as the one we sent
					Assert.That(receivedMessage1, Is.EqualTo(message), "msg1");
					Assert.That(receivedMessage2, Is.EqualTo(message), "msg2");
					Assert.That(receivedMessage3, Is.EqualTo(message), "msg3");
			} catch (IOException e) {
					Assert.That(false, "Timed out when attempting to read data");
			}

			testClient1.Close();
			testClient2.Close();
			testClient3.Close();
		}

		[Test]
		public void GivenClientDisconnectedWithNoGoodbye_DisconnectedClientIdentifiedAfterUserDataSend() {
			var testClient1 = new TcpClient(Host, Port);
			var testClient2 = new TcpClient(Host, Port);
			var testClient3 = new TcpClient(Host, Port);

			testClient1.ReceiveTimeout
					= testClient2.ReceiveTimeout
					= testClient3.ReceiveTimeout = 1000;

			Thread.Sleep(NetworkDelayTime);

			Assert.That(_server.ConnectedClientCount, Is.EqualTo(3));
			testClient3.Close();

			Thread.Sleep(NetworkDelayTime);

			//byte[] message = Encoding.ASCII.GetBytes("TestMessage");
			var packet = new Packet(Packet.MyRoutingType.Broadcast, 1, "TestMessage");
			var message = packet.Serialize();
			Thread.Sleep(NetworkDelayTime);
			testClient1.GetStream().Write(message, 0, message.Length);
			Thread.Sleep(NetworkDelayTime);

			Assert.That(_server.ConnectedClientCount, Is.EqualTo(2));

			testClient1.Close();
			testClient2.Close();
		}


		[Test]
		public void GivenClientSendsDisconnect_ServerClientCountCorrect() {
			var testClient1 = new TcpClient(Host, Port);
			var testClient2 = new TcpClient(Host, Port);
			var testClient3 = new TcpClient(Host, Port);

			testClient1.ReceiveTimeout
					= testClient2.ReceiveTimeout
					= testClient3.ReceiveTimeout = 1000;

			Thread.Sleep(NetworkDelayTime);

			Assert.That(_server.ConnectedClientCount, Is.EqualTo(3), "First client count check should have 3 connections");

			// Process the greeting packet
			var receivedGreeting = new byte[16];
			testClient1.GetStream().Read(receivedGreeting, 0, 16);
			testClient2.GetStream().Read(receivedGreeting, 0, 16);
			testClient3.GetStream().Read(receivedGreeting, 0, 16);

			Thread.Sleep(NetworkDelayTime);

			// create disco packet
			var testPacket = new Packet(Packet.MyRoutingType.Disconnect, 3, "0");
			// turn into byte array
			byte[] message = testPacket.Serialize();

			// write our disco byte array to stream
			
			testClient3.GetStream().Write(message, 0, message.Length);
			testClient3.Close();
			Thread.Sleep(NetworkDelayTime);

			Assert.That(_server.ConnectedClientCount, Is.EqualTo(2), "Second client count check should have 2 connections");

			testClient1.Close();
			testClient2.Close();
		}

		[Test]
		public void Stop_GivenServerStop_ClientsDisconnected() {

			INetClient fakeClient1 = new TcpNetClient(port: Port, host: Host);
			
			Thread.Sleep(NetworkDelayTime);
			
			_server.Stop();

			Thread.Sleep(NetworkDelayTime);

			var receivedPacket = fakeClient1.ReadData();
			Assert.That( fakeClient1.IsConnected, Is.False );
		}

		[Test]
		public void GivenIdGenerationPacketTwice_ReturnsTwoPacketsAndDoesNotReturnSameIdTwice() {
			var testClient = new TcpClient( Host, Port );

			// consume the greeting
			var receivedGreeting = new byte[16];
			testClient.GetStream().Read(receivedGreeting, 0, 16);
			var receivedGreetingPacket = new Packet( receivedGreeting );
			var clientId = int.Parse(receivedGreetingPacket.Data);

			var testPacket1 = new Packet(Packet.MyRoutingType.IdGeneration, clientId, "");
			var testPacket2 = new Packet(Packet.MyRoutingType.IdGeneration, clientId, "");

			// send/receive 1st id request
			var idReceive1 = new byte[16];
			testClient.GetStream().Write(testPacket1.Serialize(),  0, testPacket1.Serialize().Length);
			Thread.Sleep(NetworkDelayTime);
			testClient.GetStream().Read(idReceive1, 0, 16);

			// send/receive 2nd id request
			var idReceive2 = new byte[16];
			testClient.GetStream().Write(testPacket2.Serialize(), 0, testPacket1.Serialize().Length);
			Thread.Sleep(NetworkDelayTime);
			testClient.GetStream().Read(idReceive2, 0, 16);

			// turn msgs into packets
			var receivedPacket1 = new Packet(idReceive1);
			var receivedPacket2 = new Packet(idReceive2);

			// makes sure given ids are not equal
			int dummyInt;
			Assert.That(int.TryParse(receivedPacket1.Data, out dummyInt));
			Assert.That(receivedPacket1.Data, Is.Not.EqualTo(receivedPacket2.Data));
		}

		[Test]
		public void GivenIdGenerationPacket_ReturnsANonZeroId()
		{
			var testClient = new TcpClient(Host, Port);

			// consume the greeting
			var receivedGreeting = new byte[16];
			testClient.GetStream().Read(receivedGreeting, 0, 16);
			var receivedGreetingPacket = new Packet(receivedGreeting);
			var clientId = int.Parse(receivedGreetingPacket.Data);

			var testPacket1 = new Packet(Packet.MyRoutingType.IdGeneration, clientId, "");

			// send/receive 1st id request
			var idReceive1 = new byte[16];
			testClient.GetStream().Write(testPacket1.Serialize(), 0, testPacket1.Serialize().Length);
			Thread.Sleep(NetworkDelayTime);
			testClient.GetStream().Read(idReceive1, 0, 16);

			// turn msgs into packets
			var receivedPacket1 = new Packet(idReceive1);

			// makes sure given id is not zero
			Assert.That(receivedPacket1.Data, Is.Not.EqualTo("0"));
		}
	}
}
