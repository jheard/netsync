using System;
using System.Linq;
using NetSync;
using NUnit.Framework;

namespace Assets.NetSync.Tests {
	[TestFixture]
	public class TestPacket {
		private const int TestSourceId = 35746;
        private const int TestDestinationId = 12345;
		private const string Message = "hello!";

		private byte[] _data;
		private DateTime _fakeTime = new DateTime(1190, 1, 2);

		[SetUp]
		public void Setup() {
			_data = new byte[15 + Message.Length];
			// Size
			var sizeBytes = BitConverter.GetBytes((short)_data.Length);
			Buffer.BlockCopy(sizeBytes, 0, _data, 0, 2);
			// Timestamp
			var timestampBytes = BitConverter.GetBytes(_fakeTime.Ticks);
			Buffer.BlockCopy(timestampBytes, 0, _data, 3, 8);
			// Sourceid
			var sourceIdBytes = BitConverter.GetBytes(TestSourceId);
			Buffer.BlockCopy(sourceIdBytes, 0, _data, 11, 4);
            // Destinationid
            var destinationIdBytes = BitConverter.GetBytes(TestDestinationId);
            Buffer.BlockCopy(destinationIdBytes, 0, _data, 11, 4);
            // Data
			var packetData = System.Text.Encoding.ASCII.GetBytes(Message);
			Buffer.BlockCopy(packetData, 0, _data, 15, Message.Length);
		}

		[Test]
		public void GivenMessageWithPacketEnvelope_PacketGeneratesCorrectly() {
			var packet = new Packet(_data);

			Assert.That(packet.Timestamp, Is.EqualTo(_fakeTime));
			Assert.That(packet.SourceId, Is.EqualTo(TestSourceId));
            Assert.That(packet.DestinationId, Is.EqualTo(TestDestinationId));
            Assert.That(packet.Data, Is.EqualTo(Message));
		}

		[Test]
		public void GivenSimplePacket_ToByteArrayReturnsCorrect() {
			TimingSystem.Instance.CurrentTime = _fakeTime;

			var packet = new Packet(TestSourceId,TestDestinationId, Message);

			var byteArray = packet.Serialize();

			Assert.That(byteArray.SequenceEqual(_data));
		}

		[Test]
		public void NewPacketCreated_PacketHasProperTime() {
			TimingSystem.Instance.CurrentTime = _fakeTime;
			var packet = new Packet(TestSourceId, TestDestinationId, Message);

			Assert.That(packet.Timestamp, Is.EqualTo(_fakeTime));
		}
	}
}