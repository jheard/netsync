
using System;
using System.Reflection;

namespace NetSync {
	public class NsSyncMessageDelete : NsSyncMessage {
		public NsSyncMessageDelete(int wrapperId) : base(wrapperId) { }

		public override string Serialize() {
			string result = MessageTypeDelete + WrapperId + "_";
			return result;
		}
	}
}

