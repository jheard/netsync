
using System;
using System.Collections.Generic;

namespace NetSync
{
	// Wraps an INetSync object.  Used by INsMaster to
	// keep the individual INetSync synchronized.
	internal interface INsObjectHandler
	{
		// Called when a member of the wrapped INetSync changed value remotely.
		// Updates the INetSync object's member with the new value.
		// 	val - The data to synchronize
		void SyncIn(NsSyncMessageChange val);

		// Gets a list of all unsynchronized data in the wrapper.
		//   (does not mark the data as synchronized)
		IList<NsSyncMessageChange> GetUnsyncedData();

		object Value
		{
			get;
		}
	}
}

