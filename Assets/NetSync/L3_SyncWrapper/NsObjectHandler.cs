
using System;
using System.Collections.Generic;

namespace NetSync
{
	// Layer 3: Wrapper
	// Wraps up a single synced object
	public class NsObjectHandler : INsObjectHandler
	{
		public NsObjectHandler(object wrapped, int wrapperId)
		{
			_wrapped = wrapped;
			_wrapperId = wrapperId;
			_syncMemberDict = new Dictionary<string, object>();

			var type = _wrapped.GetType();
			var fieldList = type.GetFields();
			foreach(var field in fieldList)
			{
				if(Attribute.IsDefined(field, typeof(NsSyncableMemberAttribute)))
				{
					_syncMemberDict.Add(field.Name, _initVal);
				}
			}
		}

		public void SyncIn(NsSyncMessageChange val)
		{
			//_wrapped.GetType().GetField(val.MemberToSync[0]).GetType().GetMethod("Parse").Invoke(null, )
			var convertedValue = Convert.ChangeType(val.Value, _wrapped.GetType().GetField(val.MemberToSync[0]).FieldType);
			_wrapped.GetType().GetField(val.MemberToSync[0]).SetValue(_wrapped, convertedValue);
			_syncMemberDict[val.MemberToSync[0]] = convertedValue;
		}

		public IList<NsSyncMessageChange> GetUnsyncedData()
		{
			var result = new List<NsSyncMessageChange>();
			foreach (var syncMember in _syncMemberDict)
			{
				var currentValue = _wrapped.GetType().GetField(syncMember.Key).GetValue(_wrapped);
				if(currentValue == null && syncMember.Value == null) {}
				else if(
					syncMember.Value == _initVal ||
					(currentValue == null && syncMember.Value != null) ||
					(currentValue != null && syncMember.Value == null) ||
					!currentValue.Equals(syncMember.Value))
				{
					result.Add(new NsSyncMessageChange(_wrapperId, new string[] { syncMember.Key }, currentValue));
				}
			}
			return result;
		}

		public object Value
		{
			get
			{
				return _wrapped;
			}
		}

    private static object _initVal = new object();
		private int _wrapperId;
		private object _wrapped;
		private Dictionary<string, object> _syncMemberDict;
	}
}

