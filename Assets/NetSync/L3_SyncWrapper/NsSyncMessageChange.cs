
using System;
using System.Reflection;

namespace NetSync {
	/// <summary>
	/// A sync message that indicates a change to data
	/// </summary>
	public class NsSyncMessageChange : NsSyncMessage {
		public readonly string[] MemberToSync;
		public readonly object Value;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wrapperId">The id of the object to change.</param>
		/// <param name="memberToSync">The member to sync, using a hierarchical organization.</param>
		/// <param name="value">The new value of the member to sync.</param>
		public NsSyncMessageChange(int wrapperId, string[] memberToSync, object value)
			: base(wrapperId) {
			this.MemberToSync = memberToSync;
			this.Value = value;
		}

		public override string Serialize() {
			// put message type
			string result = MessageTypeChange + WrapperId + "_";
			// todo replace underscore since field names can contain underscores
			// put nodes (field address). format is "_first.second.third_"
			// field is second because there is no way to tell the end of the data except end of message, 
			// since it can contain any characters
			bool isFirst = true;
			foreach (string memberAddressNode in MemberToSync) {
				if (isFirst) {
					isFirst = false;
				} else {
					result += ".";
				}
				result += memberAddressNode;
			}
			// put value
			result += "_" + Value.ToString();
			return result;
		}
	}
}

