
using System;
using System.Reflection;

namespace NetSync {
  public class NsSyncMessageNew : NsSyncMessage {
    private readonly string _objectActivatorString;

    public NsSyncMessageNew( int wrapperId, object newSync, bool newSyncIsActivatorString ) : base( wrapperId ) {
      if( newSyncIsActivatorString ) {
        this._objectActivatorString = (string) newSync;
      } else {
        this._objectActivatorString = CreateActivatorString( newSync );
      }
    }

    /// <summary>
    /// Create a string that will create a default version of the given object
    /// </summary>
    /// <param name="toActivate">The object to create an activator string for</param>
    /// <returns>A string that can be used with reflection to create a default version of the object</returns>
    public static string CreateActivatorString( object toActivate ) {
      Type toActivateType = toActivate.GetType();
      string typeString = toActivateType.ToString();
      string assemblyString = Assembly.GetAssembly( toActivateType ).ToString();
      string activatorString = typeString + ", " + assemblyString;
      return activatorString;
    }

    public override string Serialize() {
      string result = MessageTypeNew + WrapperId + "_" + _objectActivatorString;
      return result;
    }

    /// <summary>
    /// Creates an instance of the type indicated by this message.
    /// </summary>
    /// <returns>The new object being created</returns>
    public object GenerateNewSyncableObject() {
      try {
        Type typeOfVal = Type.GetType( _objectActivatorString );
        var myNewInstance = Activator.CreateInstance( typeOfVal );
        return myNewInstance;
      } catch {
        return null;
      }
    }
  }
}

