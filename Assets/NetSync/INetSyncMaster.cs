using System;
using System.Collections.Generic;

namespace NetSync
{
	// Handles keeping data synchronized for INetSync objects.
	// Does nothing with networking.  Look elsewhere for that.
	public interface INetSyncMaster
	{
		// Add an object to keep synchronized
		void RegisterNetSynchable(object val);
		// Remove an object from being synchronized
		void UnregisterNetSynchable(object val);

		// Called when a member of a registered INetSync object has changed value remotely.
		// Updates the INetSync object's member with the new value.
		// 	val - The data to synchronize
		void SyncIn(NsSyncMessage val);

		// Gets a list of all unsynchronized data
		//   (does not mark the data as synchronized)
		IList<NsSyncMessage> GetUnsyncedData();
	}
}

