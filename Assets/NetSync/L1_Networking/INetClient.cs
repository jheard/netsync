﻿
using System;
using Assets.NetSync.L2_SyncMaster;

namespace NetSync
{
	public interface INetClient : IdGenerator
	{
		bool IsConnected { get; }
		
		void Start();
		void Stop();

		/// <summary>
		/// Dequeues earliest packet that has been received from the server
		/// </summary>
		/// <returns>A packet of data from the server. If no data found, returns packet of type NullPacket.</returns>
		Packet ReadData();

		/// <summary>
		/// Sends given data to the server
		/// Sends packet of 'user data' _myType
		/// </summary>
		/// <param name="data">String data to send to the server</param>
		void SendData(string data);
	}
}
