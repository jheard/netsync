using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Assets.NetSync.L1_Networking;

namespace NetSync {
  // Layer 1: Networking
  // Class representing the listening
  public class TcpNetServer : NetBase, INetServer {
    private readonly Dictionary<int, TcpClient> _clientDict = new Dictionary<int, TcpClient>();
    private readonly short _port;
    private bool _isConnected;
    private TcpListener _tcpListener;
    private int _uniqueIdCounter = 1;

    public TcpNetServer( short port ) {
      _port = port;
    }

    public override bool IsConnected { get { return _isConnected; } }

    public bool Start() {
      try {
        _tcpListener = new TcpListener( IPAddress.Any, _port );
        _tcpListener.Start();
        _tcpListener.BeginAcceptTcpClient( OnNewConnection, null );
        _isConnected = true;
      } catch( Exception e ) {
        return false;
      }
      return true;
    }

    public void Stop() {
      if( IsConnected ) {
        _tcpListener.Stop();
        KickAllClients();
        _isConnected = false;
      }
    }

    public int ConnectedClientCount { get { return _clientDict.Count; } }

    public override void OnGetData( Packet packet ) {
      lock( _clientDict ) {
        var disconnectedClientList = new List<int>();

        switch( packet.DestinationId ) {
          case Packet.NullDestination:
            break;

          case Packet.BroadcastDestination:
            // look for any unresponsive clients and remove
            foreach( var keyValuePair in _clientDict ) {
              SendData( keyValuePair.Value.GetStream(), packet );
              if( !keyValuePair.Value.Connected ) {
                disconnectedClientList.Add( keyValuePair.Key );
              }
            }
            foreach (int key in disconnectedClientList) {
              HandleClientDisconnect(key);
            }
            break;
          case Packet.ServerDestination:
            HandleNetworkingPacket( packet );
            break;
          default:
            SendData(_clientDict[packet.DestinationId].GetStream(), packet);
            break;
        }
      }
    }

    /// <summary>
    /// Handles packet data destined for the networking layer
    /// </summary>
    /// <param name="packet">The packet to handle the networking data for</param>
    private void HandleNetworkingPacket( Packet packet ) {
      var nsMessage = NsSyncMessage.Deserialize( packet.Data );
      
      switch( nsMessage.MessageType ) {
        case NsSyncMessage.MessageTypeDisconnect:
          if (_clientDict.ContainsKey(packet.SourceId)) {
            HandleClientDisconnect(packet.SourceId);
          }
          break;
        case NsSyncMessage.MessageTypeIdGeneration:
          var idMessage = new NsSyncMessageIdGeneration( GetUniqueId() );
          var newIdPacket = new Packet( 0, packet.SourceId, idMessage.Serialize() );
          SendData( _clientDict[ packet.SourceId ].GetStream(), newIdPacket );
          break;
        default:
          throw ( new Exception("TcpNetServer::HandleNetworkingPacket - Incorrect message type passed" ) );
      }
    }


    /// <summary>
    /// Deal with a disconnected client
    /// </summary>
    /// <param name="clientId">The id of the client to handle the disconnect for</param>
    private void HandleClientDisconnect(int clientId) {
      // todo need to broadcast disconnect on disconnect
      _clientDict[clientId].Close();
      _clientDict.Remove(clientId);
    }


    private void OnNewConnection( IAsyncResult ar ) {
      TcpClient client = null;
      try {
        client = _tcpListener.EndAcceptTcpClient( ar );
        BeginRead( client.GetStream() );
        int newClientId = GetUniqueId();
        _clientDict.Add( newClientId, client );
        // client id for packets sent from server always 0
        var greetingPacket = new Packet( 0, newClientId, new NsSyncMessageGreeting().Serialize() );
        SendData( client.GetStream(), greetingPacket );
        _tcpListener.BeginAcceptTcpClient( OnNewConnection, null );
      } catch {
        if( client != null ) { client.Close(); }
      }
    }

    private void KickAllClients() {
      lock( _clientDict ) {
        // KickClient removes items from the diction
        // Copying key list to allow removing items without invalidating the iteration
        var keyList = new List<int>( _clientDict.Keys );
        foreach( int clientId in keyList ) {
          KickClient( clientId );
          break;
        }
      }
    }

    private void KickClient( int clientId ) {
      lock( _clientDict ) {
        TcpClient tcpClient = _clientDict[ clientId ];
        if( tcpClient.Connected ) {
          // send disco message
          var discoPacket = new Packet( Packet.MyRoutingType.Disconnect, 0, "" );
          try {
            SendData( tcpClient.GetStream(), discoPacket );
          } catch( ObjectDisposedException e ) {
          }
          // close out client
          tcpClient.Close();
        }
        _clientDict.Remove( clientId );
      }
    }

    public int GetUniqueId() {
      return _uniqueIdCounter++;
    }
  }
}
