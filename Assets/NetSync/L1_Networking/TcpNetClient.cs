
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetSync {
	// Layer 1: Networking
	// Class representing the connecting client
	public class TcpNetClient : NetBase, INetClient {

		Queue<Packet> dataList = new Queue<Packet>();
		private int _myClientId { get; set; }

		private TcpClient _myTcpClient = new TcpClient();
		private String _host;
		private short _port;
		private Queue<int> _uniqueIds = new Queue<int>();
		private const int InitialUniqueIdBufferCount = 10;

		public override bool IsConnected {
			get { return _myTcpClient.Connected; }
		}

		public TcpNetClient(string host, short port) {
			_host = host;
			_port = port;
		}

		public void Start() {
			if (!this.IsConnected) {
				_myTcpClient.Connect(_host, _port);
        // assumes first message received is a greeting
				BeginRead(_myTcpClient.GetStream(), OnServerGreeting);
			}
		}

		public void Stop() {
			Packet toSend = new Packet(_myClientId, 0, new NsSyncMessageDisconnect(_myClientId).Serialize());
			byte[] buffer = toSend.Serialize();
			_myTcpClient.GetStream().Write(buffer, 0, buffer.Length);
			_myTcpClient.Close();
		}

		public override void OnGetData(Packet packet) {
			if (packet.DestinationId == _myClientId) {
        // if it's intended for me specifically, 
        // we need to check to see its type for special cases
				var message = NsSyncMessage.Deserialize(packet.Data);
			  if( message.MessageType == NsSyncMessage.MessageTypeIdGeneration ) {
			    int newId = ((NsSyncMessageIdGeneration)message).Id;
			    _uniqueIds.Enqueue( newId );
			  } else {
          dataList.Enqueue(packet);
			  }
			} else {
        // Otherwise, just queue it for later processing.
        dataList.Enqueue(packet);
			}
		}

		public Packet ReadData() {
			if (dataList.Count == 0) return Packet.NullPacket;
			return dataList.Dequeue();
		}

		public void SendData(string data) {
			Packet toSend = new Packet(Packet.MyRoutingType.Broadcast, _myClientId, data);
			byte[] buffer = toSend.Serialize();
			_myTcpClient.GetStream().Write(buffer, 0, buffer.Length);
		}


		/// <summary>
		/// Gets a unique ID by asking the server for one, then returning the result 
		/// </summary>
		/// <remarks>Expects to be attached to server, otherwise will throw</remarks>
		/// <returns></returns>
		public int GetUniqueId() {
			RequestUniqueIdPool(1);
			while (_uniqueIds.Count < 1) {
				Thread.Sleep(1);
				// sit and wait, buddy
			}
			var result = _uniqueIds.Dequeue();
			return result;
		}


		
    // 
    /// <summary>
    /// job is to receive signal, turn signal into packet, and call 'OnGetData' function
    /// assumes first message received is a greeting
    /// </summary>
    /// <remarks>This may be used in the future for receiving software related data (version type, etc)</remarks>
    /// <param name="ar"></param>
		private void OnServerGreeting(IAsyncResult ar) {
			// Get the stream
			var stream = ar.AsyncState as NetworkStream;
			Debug.Assert(stream != null, "stream != null");

			// Read the first two bytes of the packet (its size)
			var packetLengthBuffer = new byte[2];
			// ReSharper disable once PossibleNullReferenceException
			var readByteCount = stream.Read(packetLengthBuffer, 0, 2);
			// A zero messege means client disconnects.
			if (readByteCount != 0) {
				var packetLength = BitConverter.ToUInt16(packetLengthBuffer, 0);

				// Read the remaining bytes
				var packetBytes = new byte[packetLength];
				Buffer.BlockCopy(packetLengthBuffer, 0, packetBytes, 0, 2);
				stream.Read(packetBytes, 2, packetLength - 2);

				var packet = Packet.Deserialize(packetBytes);

			  var message = NsSyncMessage.Deserialize( packet.Data ) as NsSyncMessageGreeting;

				Debug.Assert(packet.Type == Packet.MyRoutingType.ServerGreeting);
				_myClientId = Int32.Parse(packet.Data);

				// called here because we need the client id set to send the request
				RequestUniqueIdPool(InitialUniqueIdBufferCount);
			}

			BeginRead(stream);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <remarks>Assumes clientid has been set to id provided by server</remarks>
		/// <param name="uniqueIdRequestCount"></param>
		private void RequestUniqueIdPool(int uniqueIdRequestCount) {
			if (!IsConnected) {
				throw (new Exception("Must be connected to server to retrieve unique ids."));
			}
			for (int i = 0; i < uniqueIdRequestCount; i++) {
				var idRequestPacket = new Packet(_myClientId, 0, new NsSyncMessageIdGeneration(_myClientId).Serialize());
				SendData(_myTcpClient.GetStream(), idRequestPacket);
			}
		}
	}
}
