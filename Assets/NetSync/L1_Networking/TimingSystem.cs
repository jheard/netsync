﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetSync {
	public class TimingSystem {
		public static TimingSystem Instance {
			get {
				if(_instance == null)
				{
					_instance = new TimingSystem();
				}
				return _instance;
			}
		}
		private static TimingSystem _instance = null;



		public DateTime CurrentTime {
			get {
				if(_currentTime == DateTime.MinValue) {
					return DateTime.Now;
				}
				return _currentTime;
			}
			set {
				_currentTime = value;
			}
		}
		private DateTime _currentTime = DateTime.MinValue;
	}
}
