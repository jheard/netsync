
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetSync {
  // Handles the networking shared by both the server and the client
  public abstract class NetBase {
    public abstract void OnGetData(Packet packet);

    public abstract bool IsConnected { get; }


    protected void SendData(NetworkStream stream, Packet packet) {
      // do not send if no data in data field (empty packets not allowed)
      byte[] buffer = packet.Serialize();
      try {
        stream.Write(buffer, 0, buffer.Length);
      } catch (Exception e) { }
    }


    protected void BeginRead(Stream stream, AsyncCallback asyncCallback = null) {
      if (asyncCallback == null) {
        asyncCallback = OnGetDataInternal;
      }
      stream.BeginRead(_dummyBuffer, 0, 0, asyncCallback, stream);
    }

    // job is to receive signal, turn signal into packet, and call 'OnGetData' function
    private void OnGetDataInternal(IAsyncResult ar) {
      // Get the stream
      var stream = ar.AsyncState as NetworkStream;
      Debug.Assert(stream != null, "stream == null");

      // if the stream is disposed, we are done, don't reset callback
      if (!stream.DataAvailable) return;

      // Read the first two bytes of the packet (its size)
      var packetLengthBuffer = new byte[2];
      // ReSharper disable once PossibleNullReferenceException
      var readByteCount = stream.Read(packetLengthBuffer, 0, 2);
      // if we should continue reading from stream
      bool stopReadingStream = false;

      // A zero messege means client disconnects.
      if (readByteCount != 0) {
        var packetLength = BitConverter.ToUInt16(packetLengthBuffer, 0);

        // Read the remaining bytes
        var packetBytes = new byte[packetLength];
        Buffer.BlockCopy(packetLengthBuffer, 0, packetBytes, 0, 2);
        stream.Read(packetBytes, 2, packetLength - 2);

        var packet = new Packet(packetBytes);
        OnGetData(packet);
        stopReadingStream = (packet.Type == Packet.MyRoutingType.Disconnect);
      } else {
        stopReadingStream = true;
      }
      if (!stopReadingStream) {
        BeginRead(stream);
      }
    }

    private readonly byte[] _dummyBuffer = new byte[0];
  }
}
