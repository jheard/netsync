
using System;
using System.Reflection;

namespace NetSync {
	public class NsSyncMessageGreeting : NsSyncMessage {

	  public readonly int AssignedId;

    // wrapper id will be ignored, since greeting messages are handled
    // at the network layer (before wrapper id used)
	  public NsSyncMessageGreeting( int assignedId ) : base( -1 ) {
	    AssignedId = assignedId;
	  }

		public override string Serialize() {
			string result = MessageTypeGreeting + WrapperId + "_" + AssignedId;
			return result;
		}
	}
}

