﻿
using System;

namespace Assets.NetSync.L1_Networking
{
	interface INetServer
	{
		int ConnectedClientCount { get; }
		bool IsConnected { get; }

		bool Start();
		void Stop();
	}
}
