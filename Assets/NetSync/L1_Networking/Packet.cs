
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

// Layer 1: Networking
// Handles the networking shared by both the server and the client

namespace NetSync {
	public class Packet {
		// numeric values above 0 indicate which client the packet is intended for
		// 0 and below are special packet destinations
		public const int ServerDestination = 0;
		public const int BroadcastDestination = -1;
		public const int NullDestination = -2;

    /// <summary>
    /// A static packet that represents a non-packet
    /// </summary>
		public static readonly Packet NullPacket = new Packet(-1, -1, "");

		/// <summary>Flag indicating that the message should be parsed by the network layer, not passed along</summary>
		public bool IsNetworkingData;
		/// <summary>When the packet is created</summary>
		public readonly DateTime Timestamp;
		/// <summary>client id for packets sent from server always 0</summary>
		public readonly int SourceId;
		/// <summary>client id for packets sent from server always 0</summary>
		public readonly int DestinationId;
		/// <summary>The data transmitted in the packet</summary>
		public readonly string Data;

	  public Packet(int sourceId, int destinationId, string data) {
			Timestamp = TimingSystem.Instance.CurrentTime;
			SourceId = sourceId;
			DestinationId = destinationId;
			Data = data;
		}

	  public Packet( int sourceId, int destinationId, string data, DateTime timeStamp ) {
      Timestamp = timeStamp;
      SourceId = sourceId;
      DestinationId = destinationId;
      Data = data;
	  }

	  public static Packet Deserialize(byte[] bytes) {
			var size = BitConverter.ToInt16(bytes, 0);
			Debug.Assert(bytes.Length == size, "Invalid packet data length.");
		  
			var timestamp = new DateTime(BitConverter.ToInt64(bytes, OffsetTimestamp));
			var sourceId = BitConverter.ToInt32(bytes, OffsetSourceId);
			var destinationId = BitConverter.ToInt32(bytes, OffsetDestinationId);
			var data = Encoding.UTF8.GetString(bytes, OffsetData, size - OffsetData);

      return new Packet( sourceId, destinationId, data, timestamp );
	  }

		public byte[] Serialize() {
			var result = new byte[Data.Length + OffsetData];
			// Size
			var packetSize = BitConverter.GetBytes(result.Length);
			Buffer.BlockCopy(packetSize, 0, result, OffsetSize, 2);
			// Timestamp
			var packetTimestamp = BitConverter.GetBytes(Timestamp.Ticks);
			Buffer.BlockCopy(packetTimestamp, 0, result, OffsetTimestamp, 8);
			// SourceId
			var packetSourceId = BitConverter.GetBytes(SourceId);
			Buffer.BlockCopy(packetSourceId, 0, result, OffsetSourceId, 4);
			// DestinationId
			var packetDestinationId = BitConverter.GetBytes(SourceId);
			Buffer.BlockCopy(packetDestinationId, 0, result, OffsetDestinationId, 4);
			// Data
			var packetData = System.Text.Encoding.ASCII.GetBytes(Data);
			Buffer.BlockCopy(packetData, 0, result, OffsetData, Data.Length);

			return result;
		}

		private const int
		OffsetSize = 0,
		OffsetTimestamp = 2,
		OffsetSourceId = 10,
					OffsetDestinationId = 14,
					OffsetData = 18;

	}
}
