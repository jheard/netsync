
using System;
using System.Reflection;

namespace NetSync {
	// Sent client->server to request a unique id from the server
	// Sent server->client to respond with a unique id
	public class NsSyncMessageIdGeneration : NsSyncMessage {
		public int Id;

    // wrapper id will be ignored, since id generation messages are handled
    // at the network layer (before wrapper id used)
		public NsSyncMessageIdGeneration() : base(-1) { }
		public NsSyncMessageIdGeneration(int id)
			: base(-1) {
			Id = id;
		}

		public override string Serialize() {
			string result = MessageTypeIdGeneration + WrapperId + "_" + Id;
			return result;
		}
	}
}

