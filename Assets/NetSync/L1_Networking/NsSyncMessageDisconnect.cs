
using System;
using System.Reflection;

namespace NetSync {
	public class NsSyncMessageDisconnect : NsSyncMessage {
		public NsSyncMessageDisconnect(int wrapperId) : base(wrapperId) { }

		public override string Serialize() {
			string result = MessageTypeDisconnect + WrapperId + "_";
			return result;
		}
	}
}

