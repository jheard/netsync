
using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Permissions;
using Assets.NetSync.L2_SyncMaster;

namespace NetSync {
	// Layer 2: Master
	// Organizes all of the synced objects for a single client
	public class NsMaster : INsMaster {
		// OnRegister event
		public event OnRegisterDelegate OnRegister;

		// list of object wrappers the master is keeping synched
		private Dictionary<int, INsObjectHandler> _synchableList = null;

		private IdGenerator _idGenerator;

		private readonly List<NsSyncMessage> _synchableExistanceMessages = new List<NsSyncMessage>();

		public NsMaster( IdGenerator idGeneratorToUse ) {
			_synchableList = new Dictionary<int, INsObjectHandler>();
			_idGenerator = idGeneratorToUse;
		}

		/// <summary>
		/// Register the given var if it has not already been registered
		/// expects that the given wrapper id is unique
		/// </summary>
		/// <param name="objectToSync"></param>
		public void RegisterSynchable(object objectToSync) {
			var matchingVals = _synchableList.Where(pair => pair.Value.Value == objectToSync);
			if (!matchingVals.Any()) {
				// we need to add it here because the actual values of the added object will be automatically synched after
				var newId = _idGenerator.GetUniqueId();
				_synchableList.Add(newId, new NsObjectHandler(objectToSync, newId));
				_synchableExistanceMessages.Add(new NsSyncMessageNew(newId, objectToSync, false));

				// synchronize actual object to be synched with any listeners
				if (OnRegister != null) {
					OnRegister(objectToSync);
				}
			}
		}

		public void UnregisterNetSynchable(object val) {
			int ourKey = _synchableList.First(s => s.Value.Value == val).Key;
			_synchableExistanceMessages.Add(new NsSyncMessageDelete(ourKey));
		}

		public void SyncIn(NsSyncMessage val) {
			// Check what kind of message we have and react accordingly
			if (val is NsSyncMessageChange) {
				_synchableList[val.WrapperId].SyncIn((NsSyncMessageChange)val);
			} else if (val is NsSyncMessageNew) {
				AddSynchableToRegistered(val);
				// check if we have this message pending and delete it
				_synchableExistanceMessages.Remove(val);
			} else if (val is NsSyncMessageDelete) {
				// create a new dictionary based on the old one without the objectToSync we are removing
				_synchableList = _synchableList.Where(pair => pair.Key != val.WrapperId)
					.ToDictionary(pair => pair.Key, pair => pair.Value);
				// remove this message from the queue if it exists
				_synchableExistanceMessages.Remove(val);
			}
		}

		// use reflection to create an object of the given _myType
		// and add it to our list of registered synchables
		private void AddSynchableToRegistered(NsSyncMessage val) {
			// Ignore new object messages that were generated locally
			if (!_synchableList.ContainsKey(val.WrapperId)) {
				// we still need the user to be able to react to getting a new object
				var myNewInstance = ((NsSyncMessageNew)val).GenerateNewSyncableObject();

				// add it to our master list with the given id
				_synchableList.Add(val.WrapperId, new NsObjectHandler(myNewInstance, val.WrapperId));

				// synchronize actual object created to be synched with any listeners
				if (OnRegister != null) {
					OnRegister(myNewInstance);
				}
			}
		}

		public List<NsSyncMessage> GetUnsyncedData() {
			var result = _synchableExistanceMessages.ToList();

			foreach (var wrapper in _synchableList.Values) {
				var unsynced = wrapper.GetUnsyncedData();
				foreach (var unsyncItem in unsynced) {
					result.Add(unsyncItem);
				}
			}

			return result;
		}
	}
}

