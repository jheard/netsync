
using System;
using System.Collections.Generic;

namespace NetSync
{
	public delegate void OnRegisterDelegate(Object syncable);

	// Handles keeping data synchronized for INetSync objects.
	// Does nothing with networking.  Look elsewhere for that.
	public interface INsMaster
	{
		// Add an object to keep synchronized
		// Used to indicate an object should be part of synched data
		void RegisterSynchable(object objectToSync);
		// Remove an object from being synchronized
		void UnregisterNetSynchable(object val);

		// Called when a member of a registered INetSync object has changed value remotely.
		// Updates the INetSync object's member with the new value.
		// 	objectToSync - The data to synchronize
		void SyncIn(NsSyncMessage val);

		// Gets a list of all unsynchronized data
		//   (does not mark the data as synchronized)
		List<NsSyncMessage> GetUnsyncedData();

		// OnRegister event
		event OnRegisterDelegate OnRegister;
	}
}

