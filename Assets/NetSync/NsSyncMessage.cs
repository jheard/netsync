
using System;
using System.Reflection;

namespace NetSync {
	// A message communicating a change in synced data.
	// It contains the location of the data that has changed and
	// the value it has changed to.
	// The location of the data is broken up into a wrapperId to
	// define the specific NetSync object that has changed and
	// the memberToSync string that is the name of the member changed.
	public abstract class NsSyncMessage {
		public const char MessageTypeNew = 'n';
		public const char MessageTypeChange = 'c';
		public const char MessageTypeDelete = 'd';
		public const char MessageTypeGreeting = 'G';
		public const char MessageTypeDisconnect = 'D';
		public const char MessageTypeIdGeneration = 'i';

    /// <summary>The type of message this is</summary>
    /// <remarks>
    /// this is a private set instead of being overridden in each subclass
    /// so that we don't have to override it in each class
    /// and don't have to delete each overridden version if the design changes
    /// </remarks>
	  public char MessageType { get; private set; }

    /// <summary>The id for the specific object being referenced in this message</summary>
		public int WrapperId;

		protected NsSyncMessage(int wrapperId) {
			this.WrapperId = wrapperId;
		}

		/// <summary>
		/// Turn this message into a string for network transfer
		/// </summary>
		/// <returns>A string that defines the message</returns>
		public abstract string Serialize();

		public override string ToString() {
			return Serialize();
		}

		public override bool Equals(Object rhs) {
			if (!(rhs is NsSyncMessage)) return false;
			if (Serialize() != ((NsSyncMessage)rhs).Serialize()) return false;
			return true;
		}

		public static NsSyncMessage Deserialize(string serializedString) {
			char messageType = serializedString[0];
			int wrapperIdEndIndex = serializedString.IndexOf('_');
			string messageValue = "";

			messageValue = serializedString.Substring(wrapperIdEndIndex + 1);
			int wrapperId = Int32.Parse(serializedString.Substring(1, wrapperIdEndIndex - 1));

      NsSyncMessage result = new NsSyncMessageDelete( 0 ){ MessageType = 'a'};

		  string errorMessage = "Error";
			// the first character indicates the message type
			// n == new, c == change, d == delete
		  switch( serializedString[ 0 ] ) {
		    case MessageTypeNew:
		      return new NsSyncMessageNew( wrapperId, messageValue, true );
		      break;
		    case MessageTypeChange:
		      // change message format is message type char, wrapper id, address, data
		      // example: c21_first.second_datadata
		      var pieces = messageValue.Split( '_' );
		      var address = pieces[ 0 ].Split( '.' );
		      var value = pieces[ 1 ];
		      result = new NsSyncMessageChange( wrapperId, address, value );
		      break;
		    case MessageTypeDelete:
		      result = new NsSyncMessageDelete( wrapperId );
		      break;
		    case MessageTypeGreeting:
          // greeting message format is message type char, wrapper id, assigned id value
          var greetingPacketDataPieces = messageValue.Split('_');
          var greetingIdString = greetingPacketDataPieces[1];
          int greetingIdNumeric;
          if (Int32.TryParse(greetingIdString, out greetingIdNumeric)) {
            result = new NsSyncMessageGreeting(greetingIdNumeric);
          } else {
            errorMessage = "NsSyncMessage::Deserialize - Received non-numeric data for greeting message";
          }
		      break;
		    case MessageTypeDisconnect:
		      result = new NsSyncMessageDisconnect( wrapperId );
		      break;
		    case MessageTypeIdGeneration:
          // id generation message format is message type char, wrapper id, id value
          var idPacketDataPieces = messageValue.Split( '_' );
		      var idString = idPacketDataPieces[ 1 ];
		      int idNumeric;
		      if( Int32.TryParse( idString, out idNumeric ) ) {
		        result = new NsSyncMessageIdGeneration( idNumeric );
		      } else {
		        errorMessage = "NsSyncMessage::Deserialize - Received non-numeric data for id generation message";
		      }
		      break;
		    default:
		      result = null;
		      break;
		  }
		  if( result != null ) {
		    result.MessageType = serializedString[ 0 ];
		  } else {
		    throw ( new Exception(errorMessage) );
		  }
		  return result;
		}
	}
}

