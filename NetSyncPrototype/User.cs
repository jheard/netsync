﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSync;

namespace NetSyncPrototype {
	class User {
		// set up some variables to be synchable
		[NsSyncableMember]
		public string Name;
		[NsSyncableMember]
		public int Amount;
		[NsSyncableMember]
		public bool TestFlag;
		[NsSyncableMember]
		public double TestDouble;
	}
}
