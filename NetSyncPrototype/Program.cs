﻿using System;
using System.Collections.Generic;
using System.Threading;
using NetSync;

namespace NetSyncPrototype {
	class Program {
		static void Main( string[] args ) {
			var program = new Program();
		    program.Run();
		}

		private readonly List<User> _mRegisteredUsers = new List<User>(); 

		public void Run() {
			// ask user if server or client
			Console.WriteLine( "1 for server, 2 for client" );
			var input = Console.ReadLine();
			var isServer = input == "1";

			// if client, ask for host
			string host = "";
			if( !isServer ) {
				Console.WriteLine( "Enter host" );
				host = Console.ReadLine();
			}

			// if server or client (any)
			// ask for port
			Console.WriteLine( "Enter port" );
			var port = Console.ReadLine();

			// setup nscontrol, using host and port
			NsControlTcp control;
		    // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
			if( isServer ) {
				control = new NsControlTcp( short.Parse( port ) );
			} else {
				control = new NsControlTcp( host, short.Parse( port ) );
			}
			control.Start();

			// set up callback for when items are registered
			control.OnRegister += OnNsRegister;

			bool quit = false;
			while ( !quit ) {
				control.ReadFromNet();

				// display options
				Console.WriteLine( "Choose from the following options:" );
				Console.WriteLine( "1. View all users" );
				Console.WriteLine( "2. Edit user" );
				Console.WriteLine( "3. Quit" );
				Console.WriteLine( "4. Register new thing" );
				Console.WriteLine("5. Read/Write from net");
				input = Console.ReadLine();

				switch( input ) {
						// user can view all user objects
					case "1":
						ViewUsers();
						break;
						// user can edit any user objects
					case "2":
						ViewUsers();
						EditUsers();
						break;
					case "3":
						quit = true;
						break;
					case "4":
						control.RegisterSyncable( new User(){Name = "newuser"} );
						break;
					case "5":
						break;
					default:
						Console.WriteLine( "Invalid option" );
						break;
				}
				Console.WriteLine("------------------");

				
				control.WriteToNet();
			}
		}

		private void EditUsers() {
			Console.WriteLine("Which user would you like to edit?");
			var userIndex = Int32.Parse( Console.ReadLine() );
			Console.WriteLine( "Enter Name" );
			var newName = Console.ReadLine();
			Console.WriteLine( "Enter Amount" );
			var newAmount = Int32.Parse(Console.ReadLine());
			Console.WriteLine("Enter TestFlag (true/false)");
			var inputFlag = bool.Parse(Console.ReadLine());
			Console.WriteLine("Enter TestDouble");
			var inputDouble = Double.Parse(Console.ReadLine());
			_mRegisteredUsers[ userIndex ].Name = newName;
			_mRegisteredUsers[ userIndex ].Amount = newAmount;
			_mRegisteredUsers[ userIndex ].TestFlag = inputFlag;
			_mRegisteredUsers[ userIndex ].TestDouble = inputDouble;
		}

		private void ViewUsers() {
			int index = 0;
			foreach (var user in _mRegisteredUsers) {
				Console.WriteLine("Key: " + index);
				Console.WriteLine("\tUser name: " + user.Name);
				Console.WriteLine("\tUser amount: " + user.Amount);
				Console.WriteLine("\tUser testFlag: " + user.TestFlag);
				Console.WriteLine("\tUser testDouble: " + user.TestDouble);
				Console.WriteLine();
				index++;
			}
		}

		public void OnNsRegister(object registered) {
			_mRegisteredUsers.Add( (User) registered );
		}
	}
}
